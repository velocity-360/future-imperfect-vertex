import React, { Component } from 'react'

export default (props) => {

	return (
	<div id="wrapper">

		<header id="header">
			<h1><a href="#">Future Imperfect</a></h1>
			<nav className="links">
				<ul>
					<li><a href="#">Lorem</a></li>
					<li><a href="#">Ipsum</a></li>
					<li><a href="#">Feugiat</a></li>
					<li><a href="#">Tempus</a></li>
					<li><a href="#">Adipiscing</a></li>
				</ul>
			</nav>
			<nav className="main">
				<ul>
					<li className="search">
						<a className="fa-search" href="#search">Search</a>
						<form id="search" method="get" action="#">
							<input type="text" name="query" placeholder="Search" />
						</form>
					</li>
					<li className="menu">
						<a className="fa-bars" href="#menu">Menu</a>
					</li>
				</ul>
			</nav>
		</header>

		<section id="menu">

			<section>
				<form className="search" method="get" action="#">
					<input type="text" name="query" placeholder="Search" />
				</form>
			</section>

			<section>
				<ul className="links">
					<li>
						<a href="#">
							<h3>Lorem ipsum</h3>
							<p>Feugiat tempus veroeros dolor</p>
						</a>
					</li>
					<li>
						<a href="#">
							<h3>Dolor sit amet</h3>
							<p>Sed vitae justo condimentum</p>
						</a>
					</li>
					<li>
						<a href="#">
							<h3>Feugiat veroeros</h3>
							<p>Phasellus sed ultricies mi congue</p>
						</a>
					</li>
					<li>
						<a href="#">
							<h3>Etiam sed consequat</h3>
							<p>Porta lectus amet ultricies</p>
						</a>
					</li>
				</ul>
			</section>

			<section>
				<ul className="actions vertical">
					<li><a href="#" className="button big fit">Log In</a></li>
				</ul>
			</section>
		</section>

			<div id="main">

					<article className="post">
						<header>
							<div className="title">
								<h2><a href="#">Magna sed adipiscing</a></h2>
								<p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
							</div>
							<div className="meta">
								<time className="published" datetime="2015-11-01">November 1, 2015</time>
								<a href="#" className="author"><span className="name">Jane Doe</span><img src="/images/avatar.jpg" alt="" /></a>
							</div>
						</header>
						<a href="#" className="image featured"><img src="/images/pic01.jpg" alt="" /></a>
						<p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
						<footer>
							<ul className="actions">
								<li><a href="#" className="button big">Continue Reading</a></li>
							</ul>
							<ul className="stats">
								<li><a href="#">General</a></li>
								<li><a href="#" className="icon fa-heart">28</a></li>
								<li><a href="#" className="icon fa-comment">128</a></li>
							</ul>
						</footer>
					</article>

					<article className="post">
						<header>
							<div className="title">
								<h2><a href="#">Ultricies sed magna euismod enim vitae gravida</a></h2>
								<p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
							</div>
							<div className="meta">
								<time className="published" datetime="2015-10-25">October 25, 2015</time>
								<a href="#" className="author"><span className="name">Jane Doe</span><img src="/images/avatar.jpg" alt="" /></a>
							</div>
						</header>
						<a href="#" className="image featured"><img src="/images/pic02.jpg" alt="" /></a>
						<p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper.</p>
						<footer>
							<ul className="actions">
								<li><a href="#" className="button big">Continue Reading</a></li>
							</ul>
							<ul className="stats">
								<li><a href="#">General</a></li>
								<li><a href="#" className="icon fa-heart">28</a></li>
								<li><a href="#" className="icon fa-comment">128</a></li>
							</ul>
						</footer>
					</article>

					<article className="post">
						<header>
							<div className="title">
								<h2><a href="#">Euismod et accumsan</a></h2>
								<p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
							</div>
							<div className="meta">
								<time className="published" datetime="2015-10-22">October 22, 2015</time>
								<a href="#" className="author"><span className="name">Jane Doe</span><img src="/images/avatar.jpg" alt="" /></a>
							</div>
						</header>
						<a href="#" className="image featured"><img src="/images/pic03.jpg" alt="" /></a>
						<p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla. Cras vehicula tellus eu ligula viverra, ac fringilla turpis suscipit. Quisque vestibulum rhoncus ligula.</p>
						<footer>
							<ul className="actions">
								<li><a href="#" className="button big">Continue Reading</a></li>
							</ul>
							<ul className="stats">
								<li><a href="#">General</a></li>
								<li><a href="#" className="icon fa-heart">28</a></li>
								<li><a href="#" className="icon fa-comment">128</a></li>
							</ul>
						</footer>
					</article>

					<article className="post">
						<header>
							<div className="title">
								<h2><a href="#">Elements</a></h2>
								<p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
							</div>
							<div className="meta">
								<time className="published" datetime="2015-10-18">October 18, 2015</time>
								<a href="#" className="author"><span className="name">Jane Doe</span><img src="images/avatar.jpg" alt="" /></a>
							</div>
						</header>

						<section>
							<h3>Lists</h3>
							<div className="row">
								<div className="6u 12u$(medium)">
									<h4>Unordered</h4>
									<ul>
										<li>Dolor pulvinar etiam.</li>
										<li>Sagittis adipiscing.</li>
										<li>Felis enim feugiat.</li>
									</ul>
									<h4>Alternate</h4>
									<ul className="alt">
										<li>Dolor pulvinar etiam.</li>
										<li>Sagittis adipiscing.</li>
										<li>Felis enim feugiat.</li>
									</ul>
								</div>
								<div className="6u$ 12u$(medium)">
									<h4>Ordered</h4>
									<ol>
										<li>Dolor pulvinar etiam.</li>
										<li>Etiam vel felis viverra.</li>
										<li>Felis enim feugiat.</li>
										<li>Dolor pulvinar etiam.</li>
										<li>Etiam vel felis lorem.</li>
										<li>Felis enim et feugiat.</li>
									</ol>
									<h4>Icons</h4>
									<ul className="icons">
										<li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
										<li><a href="#" className="icon fa-facebook"><span className="label">Facebook</span></a></li>
										<li><a href="#" className="icon fa-instagram"><span className="label">Instagram</span></a></li>
										<li><a href="#" className="icon fa-github"><span className="label">Github</span></a></li>
									</ul>
								</div>
							</div>
							<h3>Actions</h3>
							<div className="row">
								<div className="6u 12u$(medium)">
									<ul className="actions">
										<li><a href="#" className="button">Default</a></li>
										<li><a href="#" className="button">Default</a></li>
										<li><a href="#" className="button">Default</a></li>
									</ul>
									<ul className="actions small">
										<li><a href="#" className="button small">Small</a></li>
										<li><a href="#" className="button small">Small</a></li>
										<li><a href="#" className="button small">Small</a></li>
									</ul>
									<ul className="actions vertical">
										<li><a href="#" className="button">Default</a></li>
										<li><a href="#" className="button">Default</a></li>
										<li><a href="#" className="button">Default</a></li>
									</ul>
									<ul className="actions vertical small">
										<li><a href="#" className="button small">Small</a></li>
										<li><a href="#" className="button small">Small</a></li>
										<li><a href="#" className="button small">Small</a></li>
									</ul>
								</div>
								<div className="6u 12u$(medium)">
									<ul className="actions vertical">
										<li><a href="#" className="button fit">Default</a></li>
										<li><a href="#" className="button fit">Default</a></li>
									</ul>
									<ul className="actions vertical small">
										<li><a href="#" className="button small fit">Small</a></li>
										<li><a href="#" className="button small fit">Small</a></li>
									</ul>
								</div>
							</div>
						</section>

						<section>
							<h3>Table</h3>
							<h4>Default</h4>
							<div className="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Item One</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Two</td>
											<td>Vis ac commodo adipiscing arcu aliquet.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Three</td>
											<td> Morbi faucibus arcu accumsan lorem.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Four</td>
											<td>Vitae integer tempus condimentum.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Five</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2"></td>
											<td>100.00</td>
										</tr>
									</tfoot>
								</table>
							</div>

							<h4>Alternate</h4>
							<div className="table-wrapper">
								<table className="alt">
									<thead>
										<tr>
											<th>Name</th>
											<th>Description</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Item One</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Two</td>
											<td>Vis ac commodo adipiscing arcu aliquet.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Three</td>
											<td> Morbi faucibus arcu accumsan lorem.</td>
											<td>29.99</td>
										</tr>
										<tr>
											<td>Item Four</td>
											<td>Vitae integer tempus condimentum.</td>
											<td>19.99</td>
										</tr>
										<tr>
											<td>Item Five</td>
											<td>Ante turpis integer aliquet porttitor.</td>
											<td>29.99</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="2"></td>
											<td>100.00</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>

						<section>
							<h3>Buttons</h3>
							<ul className="actions">
								<li><a href="#" className="button big">Big</a></li>
								<li><a href="#" className="button">Default</a></li>
								<li><a href="#" className="button small">Small</a></li>
							</ul>
							<ul className="actions fit">
								<li><a href="#" className="button fit">Fit</a></li>
								<li><a href="#" className="button fit">Fit</a></li>
								<li><a href="#" className="button fit">Fit</a></li>
							</ul>
							<ul className="actions fit small">
								<li><a href="#" className="button fit small">Fit + Small</a></li>
								<li><a href="#" className="button fit small">Fit + Small</a></li>
								<li><a href="#" className="button fit small">Fit + Small</a></li>
							</ul>
							<ul className="actions">
								<li><a href="#" className="button icon fa-download">Icon</a></li>
								<li><a href="#" className="button icon fa-upload">Icon</a></li>
								<li><a href="#" className="button icon fa-save">Icon</a></li>
							</ul>
							<ul className="actions">
								<li><span className="button disabled">Disabled</span></li>
								<li><span className="button disabled icon fa-download">Disabled</span></li>
							</ul>
						</section>

						<section>
							<h3>Form</h3>
							<form method="post" action="#">
								<div className="row uniform">
									<div className="6u 12u$(xsmall)">
										<input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />
									</div>
									<div className="6u$ 12u$(xsmall)">
										<input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />
									</div>
									<div className="12u$">
										<div className="select-wrapper">
											<select name="demo-category" id="demo-category">
												<option value="">- Category -</option>
												<option value="1">Manufacturing</option>
												<option value="1">Shipping</option>
												<option value="1">Administration</option>
												<option value="1">Human Resources</option>
											</select>
										</div>
									</div>
									<div className="4u 12u$(small)">
										<input type="radio" id="demo-priority-low" name="demo-priority" checked/ >
										<label for="demo-priority-low">Low</label>
									</div>
									<div className="4u 12u$(small)">
										<input type="radio" id="demo-priority-normal" name="demo-priority" />
										<label for="demo-priority-normal">Normal</label>
									</div>
									<div className="4u$ 12u$(small)">
										<input type="radio" id="demo-priority-high" name="demo-priority" />
										<label for="demo-priority-high">High</label>
									</div>
									<div className="6u 12u$(small)">
										<input type="checkbox" id="demo-copy" name="demo-copy" />
										<label for="demo-copy">Email me a copy</label>
									</div>
									<div className="6u$ 12u$(small)">
										<input type="checkbox" id="demo-human" name="demo-human" checked />
										<label for="demo-human">Not a robot</label>
									</div>
									<div className="12u$">
										<textarea name="demo-message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
									</div>
									<div className="12u$">
										<ul className="actions">
											<li><input type="submit" value="Send Message" /></li>
											<li><input type="reset" value="Reset" /></li>
										</ul>
									</div>
								</div>
							</form>
						</section>

						<section>
							<h3>Image</h3>
							<h4>Fit</h4>
							<div className="box alt">
								<div className="row uniform">
									<div className="12u$"><span className="image fit"><img src="images/pic02.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic04.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic05.jpg" alt="" /></span></div>
									<div className="4u$"><span className="image fit"><img src="images/pic06.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic06.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic04.jpg" alt="" /></span></div>
									<div className="4u$"><span className="image fit"><img src="images/pic05.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic05.jpg" alt="" /></span></div>
									<div className="4u"><span className="image fit"><img src="images/pic06.jpg" alt="" /></span></div>
									<div className="4u$"><span className="image fit"><img src="images/pic04.jpg" alt="" /></span></div>
								</div>
							</div>
							<h4>Left &amp; Right</h4>
							<p><span className="image left"><img src="images/pic07.jpg" alt="" /></span>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>
							<p><span className="image right"><img src="images/pic04.jpg" alt="" /></span>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>
						</section>

					</article>

				<ul className="actions pagination">
					<li><a href="" className="disabled button big previous">Previous Page</a></li>
					<li><a href="#" className="button big next">Next Page</a></li>
				</ul>

			</div>

			<section id="sidebar">

				<section id="intro">
					<a href="#" className="logo"><img src="/images/logo.jpg" alt="" /></a>
					<header>
						<h2>Future Imperfect</h2>
						<p>Another fine responsive site template by <a href="http://html5up.net">HTML5 UP</a></p>
					</header>
				</section>

					<section>
						<div className="mini-posts">

							<article className="mini-post">
								<header>
									<h3><a href="#">Vitae sed condimentum</a></h3>
									<time className="published" datetime="2015-10-20">October 20, 2015</time>
									<a href="#" className="author"><img src="/images/avatar.jpg" alt="" /></a>
								</header>
								<a href="#" className="image"><img src="/images/pic04.jpg" alt="" /></a>
							</article>

							<article className="mini-post">
								<header>
									<h3><a href="#">Rutrum neque accumsan</a></h3>
									<time className="published" datetime="2015-10-19">October 19, 2015</time>
									<a href="#" className="author"><img src="/images/avatar.jpg" alt="" /></a>
								</header>
								<a href="#" className="image"><img src="/images/pic05.jpg" alt="" /></a>
								</article>

							<article className="mini-post">
								<header>
									<h3><a href="#">Odio congue mattis</a></h3>
									<time className="published" datetime="2015-10-18">October 18, 2015</time>
									<a href="#" className="author"><img src="/images/avatar.jpg" alt="" /></a>
								</header>
								<a href="#" className="image"><img src="/images/pic06.jpg" alt="" /></a>
							</article>

							<article className="mini-post">
								<header>
									<h3><a href="#">Enim nisl veroeros</a></h3>
									<time className="published" datetime="2015-10-17">October 17, 2015</time>
									<a href="#" className="author"><img src="/images/avatar.jpg" alt="" /></a>
								</header>
								<a href="#" className="image"><img src="/images/pic07.jpg" alt="" /></a>
							</article>

						</div>
					</section>

					<section>
						<ul className="posts">
							<li>
								<article>
									<header>
										<h3><a href="#">Lorem ipsum fermentum ut nisl vitae</a></h3>
										<time className="published" datetime="2015-10-20">October 20, 2015</time>
									</header>
									<a href="#" className="image"><img src="/images/pic08.jpg" alt="" /></a>
								</article>
							</li>
							<li>
								<article>
									<header>
										<h3><a href="#">Convallis maximus nisl mattis nunc id lorem</a></h3>
										<time className="published" datetime="2015-10-15">October 15, 2015</time>
									</header>
									<a href="#" className="image"><img src="/images/pic09.jpg" alt="" /></a>
								</article>
							</li>
							<li>
								<article>
									<header>
										<h3><a href="#">Euismod amet placerat vivamus porttitor</a></h3>
										<time className="published" datetime="2015-10-10">October 10, 2015</time>
									</header>
									<a href="#" className="image"><img src="/images/pic10.jpg" alt="" /></a>
								</article>
							</li>
							<li>
								<article>
									<header>
										<h3><a href="#">Magna enim accumsan tortor cursus ultricies</a></h3>
										<time className="published" datetime="2015-10-08">October 8, 2015</time>
									</header>
									<a href="#" className="image"><img src="/images/pic11.jpg" alt="" /></a>
								</article>
							</li>
							<li>
								<article>
									<header>
										<h3><a href="#">Congue ullam corper lorem ipsum dolor</a></h3>
										<time className="published" datetime="2015-10-06">October 7, 2015</time>
									</header>
									<a href="#" className="image"><img src="/images/pic12.jpg" alt="" /></a>
								</article>
							</li>
						</ul>
					</section>

				<section className="blurb">
					<h2>About</h2>
					<p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod amet placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at phasellus sed ultricies.</p>
					<ul className="actions">
						<li><a href="#" className="button">Learn More</a></li>
					</ul>
				</section>

				<section id="footer">
					<ul className="icons">
						<li><a href="#" className="fa-twitter"><span className="label">Twitter</span></a></li>
						<li><a href="#" className="fa-facebook"><span className="label">Facebook</span></a></li>
						<li><a href="#" className="fa-instagram"><span className="label">Instagram</span></a></li>
						<li><a href="#" className="fa-rss"><span className="label">RSS</span></a></li>
						<li><a href="#" className="fa-envelope"><span className="label">Email</span></a></li>
					</ul>
					<p className="copyright">&copy; Untitled. Design: <a href="http://html5up.net">HTML5 UP</a>. Images: <a href="http://unsplash.com">Unsplash</a>.</p>
				</section>
			</section>
	</div>

	)

}