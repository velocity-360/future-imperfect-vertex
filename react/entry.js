import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Theme from './theme/Theme'


ReactDOM.render(<Theme />, document.getElementById('root'))